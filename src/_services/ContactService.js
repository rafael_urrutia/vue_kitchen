import http from "../http-common";

class ContactService {
  create(data) {
    return http.post("/newsletter", data);
  }
}

export default new ContactService();
