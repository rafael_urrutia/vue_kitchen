module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: `
                @import '@/assets/scss/settings/_settings';
                @import '@/assets/scss/tools/_functions';
                @import '@/assets/scss/tools/_mixins';
                @import '@/assets/scss/generic/_generic';
                @import '@/assets/scss/keyframes/_keyframes';
                @import '@/assets/scss/objects/_o-animation';
                @import '@/assets/scss/pages/_p-home';
                @import '@/assets/scss/components/_c-input';
                @import '@/assets/scss/components/_c-overlay';
                @import '@/assets/scss/components/_c-sidebar';
                @import '@/assets/scss/components/_c-buttons';
                @import '@/assets/scss/components/_c-article';
                @import '@/assets/scss/components/_c-home-title';
                @import '@/assets/scss/components/_c-social-link';
                @import '@/assets/scss/utilities/_utilities';
                `,
      },
    },
  },
};
